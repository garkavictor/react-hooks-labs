import React, {useEffect, useMemo, useReducer} from 'react';
import axios from "axios";
import List from "./List";
import {useFormInput} from "../hooks/forms";

const Todo = props => {
    const todoInput = useFormInput();
    const dbUrl = 'https://test-18318.firebaseio.com/todos.json';

    const todoListReducer = (state, action) => {
        switch (action.type) {
            case 'ADD':
                return state.concat(action.payload);
            case 'SET':
                return action.payload;
            case 'REMOVE':
                return state.filter(todo => todo.id !== action.payload);
            default:
                return state;
        }
    };

    const [todoList, dispatch] = useReducer(todoListReducer, []);

    useEffect(() => {
        axios
            .get(dbUrl)
            .then(result => {
                console.log(result);
                const todoData = result.data;
                const todos = [];
                for (const key in todoData) {
                    todos.push({id: key, name: todoData[key].name})
                }
                dispatch({type: 'SET', payload: todos});
            });

        return () => {
            console.log('Cleanup');
        };
    }, []);

    const todoAddHandler = () => {
        const todoName = todoInput.value;

        axios
            .post(dbUrl, {name: todoName})
            .then(res => {
                setTimeout(() => {
                    console.log(res);
                    const todoItem = {id: res.data.name, name: todoName};
                    dispatch({type: 'ADD', payload: todoItem});
                }, 3000)
            })
            .catch(console.log);
    };

    const todoRemoveHandler = id => {
        axios.delete(`https://test-18318.firebaseio.com/todos/${id}.json`).then(res => {
            console.log(res);
            dispatch({type: 'REMOVE', payload: id});
        }).catch(console.log);
    };

    return <>
        <input type="text"
               placeholder="Todo"
               onChange={todoInput.onChange}
               style={{backgroundColor: todoInput.isValid ? 'transparent' : 'red'}}
               value={todoInput.value}/>
        <button type="button" onClick={todoAddHandler}>Add</button>
        {useMemo(() => <List items={todoList} onItemClick={todoRemoveHandler}/>, [todoList])}
    </>;
};

export default Todo;