import {useState} from 'react';

export const useFormInput = () => {
    const [value, setValue] = useState('');
    const [isValid, setIsValid] = useState(false);

    const inputChangeHandler = event => {
        setValue(event.target.value);
        setIsValid(event.target.value.trim() !== '');
    };

    return {value: value, onChange: inputChangeHandler, isValid};
};