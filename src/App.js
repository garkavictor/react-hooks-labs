import React, {useState} from 'react';

import Todo from './components/Todo';
import Header from "./components/Header";
import Auth from "./components/Auth";
import AuthContext from "./auth-context";

const App = props => {
    const [page, setPage] = useState('auth');
    const [authStatus, setAuthStatus] = useState(false);

    const login = () => {
        setAuthStatus(true);
    };

    return (
        <div className="App">
            <AuthContext.Provider value={{status: authStatus, login: login}}>
                <Header onLoadTodos={() => setPage('todos')}
                        onLoadAuth={() => setPage('auth')}/>
                <hr/>
                {page === 'todos' ? <Todo/> : null}
                {page === 'auth' ? <Auth/> : null}
            </AuthContext.Provider>
        </div>
    );
};

export default App;
